import Adafruit_DHT
import time

sensor = Adafruit_DHT.AM2302
pin = 2

while True:
    humidity, temp = Adafruit_DHT.read_retry(sensor, pin)

    if humidity is not None and temp is not None:
        print("Temp: " + str(int(temp)) + "°C Humdity: " + str(humidity) + "%")

    time.sleep(2)